FROM gradle:8.7-jdk17-alpine as builder
WORKDIR /build

# 그래들 파일이 변경되었을 때만 새롭게 의존패키지 다운로드 받게함.
COPY build.gradle settings.gradle /build/
RUN gradle build -x test --parallel --continue > /dev/null 2>&1 || true

# 빌더 이미지에서 애플리케이션 빌드
COPY . /build
RUN gradle build -x test --parallel

# Build 에 openjdk 17 이미지를 사용..
FROM openjdk:17-alpine

#work directory 지정(상대경로 추가)
WORKDIR /usr/src/app

#ARG 는 여러번 사용되는 문자열이나 숫자를 변수로 만들어 주는 속성임
ARG JAR_PATH=./build/libs

#컨테이너 외부의 파일을 컨테이너 내부로 복사
COPY --from=builder /build/build/libs/demoweb-0.0.1-SNAPSHOT.jar  ${JAR_PATH}/demoweb-0.0.1-SNAPSHOT.jar

#컨테이너를 실행했을때 실행할 command
ENTRYPOINT ["java","-jar","./build/libs/demoweb-0.0.1-SNAPSHOT.jar"]




