package com.sps.demoweb.springboot.web.controller;

import com.sps.demoweb.springboot.web.domain.LottoEntity;
import com.sps.demoweb.springboot.web.service.LottoService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Controller
@RequiredArgsConstructor
public class IndexController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private LottoService service;
//    List<Map<String, Integer>> mLottoNumList = new ArrayList<>();

    @GetMapping("/")
    public String indexOpen() {
       return "lotto";
    }
    @GetMapping("/lotto")
    public String lottoAddressControl() {
        return "lotto";
    }

    @PostMapping("/post/lotto")
    public String method(@RequestParam(value="lottoArr[]") List<Integer> lottoNumList,
                         HttpServletRequest request) {
        for(int i =0; i<lottoNumList.size(); i++){
            logger.info("POST 테스트 : " +lottoNumList.get(i));
        }
        LottoEntity lottoEntity = new LottoEntity();
        lottoEntity.setOne(lottoNumList.get(0)+"");
        lottoEntity.setTwo(lottoNumList.get(1)+"");
        lottoEntity.setThree(lottoNumList.get(2)+"");
        lottoEntity.setFour(lottoNumList.get(3)+"");
        lottoEntity.setFive(lottoNumList.get(4)+"");
        lottoEntity.setSix(lottoNumList.get(5)+"");

//        Map<String, Integer> lotto = new HashMap<>();
//        lotto.put("one",lottoNumList.get(0));
//        lotto.put("two",lottoNumList.get(1));
//        lotto.put("three",lottoNumList.get(2));
//        lotto.put("four",lottoNumList.get(3));
//        lotto.put("five",lottoNumList.get(4));
//        lotto.put("six",lottoNumList.get(5));
//        mLottoNumList.add(lotto);
        service.saveLottNum(lottoEntity);
        return "redirect:/lotto/ajax" ;
    }

    @GetMapping("/lotto/ajax")
    public String lottoAjax(Model model) {
        logger.info("테스트 redirect lottAjax");
        List<LottoEntity> lottoNumResultList = service.getAllLottoNum();
        List<Map<String, Integer>> lottoNumList = new ArrayList<>();

        for(int i =0; i<lottoNumResultList.size(); i++){
            Map <String,Integer> lottoMap = new HashMap<>();
            LottoEntity lottoEntity = lottoNumResultList.get(i);

            lottoMap.put("one",Integer.parseInt(lottoEntity.getOne()));
            lottoMap.put("two",Integer.parseInt(lottoEntity.getTwo()));
            lottoMap.put("three", Integer.parseInt(lottoEntity.getThree()));
            lottoMap.put("four", Integer.parseInt(lottoEntity.getFour()));
            lottoMap.put("five", Integer.parseInt(lottoEntity.getFive()));
            lottoMap.put("six", Integer.parseInt(lottoEntity.getSix()));
            lottoNumList.add(lottoMap);
        }

        model.addAttribute("lottoNumList",lottoNumList);
        return "lotto :: lottoTable";
    }

}
