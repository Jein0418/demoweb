package com.sps.demoweb.springboot.web.domain;

import org.springframework.data.jpa.repository.JpaRepository;

public interface LottoRepository extends JpaRepository<LottoEntity, Integer> {

}
