package com.sps.demoweb.springboot.web.domain;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import lombok.Data;

@Entity
@Data
@Table(name="Lotto")
public class LottoEntity extends BaseEntity {
    @Column
    private String one;

    @Column
    private String two;

    @Column
    private String three;

    @Column
    private String four;

    @Column
    private String five;

    @Column
    private String six;

}
