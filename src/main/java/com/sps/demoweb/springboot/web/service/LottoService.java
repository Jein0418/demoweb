package com.sps.demoweb.springboot.web.service;

import com.sps.demoweb.springboot.web.domain.LottoEntity;
import com.sps.demoweb.springboot.web.domain.LottoRepository;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class LottoService {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final LottoRepository repository;

    public void saveLottNum(LottoEntity lottoEntity){
        repository.save(lottoEntity);
    }

    public List<LottoEntity> getAllLottoNum (){
        return repository.findAll();
    }

}
